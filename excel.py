import openpyxl
import re
import pprint


def menu():

    print("\nSelecciona la opcion que desee")
    print("Nota: En modo de prueba y a causa de que el excel contiene muchos datos solo se muestran los 20 primeros")
    print("PD: En la data de error se muestra en que fila del excel el usuario debe corregir la data")
    print ("\t1 - Mostrar data limpia ")
    print ("\t2 - Mostrar data de errores ")
    print ("\t3 - Mostrar por categoria")
    print ("\t4 - Mostrar tamaño de arreglos ")
    print ("\t5 - Salir")

def menuopciones(dataclear,datadirty,cat_separada):
    while True:
        menu()

        opcionMenu = input('Digite la opcion que desea escoger: ')

        if opcionMenu=="1":
            imprimirData(dataclear)
        elif opcionMenu=="2":
            imprimirData(datadirty)
        elif opcionMenu=="3":
            mostrar_categorias(cat_separada)
        elif opcionMenu =="4":
            tamanio_arreglos(dataclear,datadirty,cat_separada)
        elif opcionMenu =="5":
            break
        else:
            input("Ud ha digitado una opcion incorrecta...\npulse cualquier tecla e intente nuevamente: ")


def is_email(email):
    regex = r"(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])"
    return re.match(regex, str(email)) is not None

def es_entero(row_id):
    return isinstance(row_id, int)

def es_float(sales):
    return isinstance(sales, float)

class ReadFile:

    def __init__(self,path):
        self.path = path

    def read_file(self):
        wb_obj= openpyxl.load_workbook(self.path)
        sheet_obj = wb_obj.active
        first_row = []

        for col in range(sheet_obj.max_column):
            first_row.append(sheet_obj.cell(1, col + 1).value)
        data = []

        for row in range(2, sheet_obj.max_row+ 1):
            elm = {}
            for col in range(sheet_obj.max_column):
                elm[first_row[col]] = sheet_obj.cell(row, col + 1).value
            data.append(elm)
        return data

    def clean_data(self, data):
        clean_data  = []
        trash_data  = []
        count=1
        for items in data:
            count +=1
            if es_entero(items.get('row_id')) and is_email(items.get('email')) and es_float(items.get('sales')):
                clean_data.append(items)
            else:
                trash_data.append(items)
                items['Fila a cambiar']=count
        return clean_data, trash_data

    def separar_categoria(self, data):
        main_data_for_ship  = {}
        for items in data:
            if main_data_for_ship.get(items.get('ship_mode')):
                main_data_for_ship.get(items.get('ship_mode')).append(items)
            else:
                main_data_for_ship[items.get('ship_mode')]=[items]
        return main_data_for_ship

def imprimirData(data):
    pprint.pprint(data[:20])

def mostrar_categorias(cat_separada):
    for key,value in cat_separada.items():
        print(key)
        pprint.pprint(value[:20])
        print(" ")

def tamanio_arreglos(clear, dirty, category):
    print('Data Clean: ',len(clear))
    print('Data trash:', len(dirty))

    for key, value in category.items():
        print(key,':',len(value))



def main():
    test = ReadFile('/home/lsv/Escritorio/Practica Python/ejemplo.xlsx')
    data= test.read_file()
    cat_separada=test.separar_categoria(data)
    dataclear, datadirty = test.clean_data(data)
    menuopciones(dataclear,datadirty,cat_separada)




if __name__ == '__main__':
    main()

